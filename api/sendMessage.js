const client = require("twilio")(
    "AC38b0b8cbe8c183e5ae519a4055517747",
    "4c11022fe27df89a0423cdb1f8951ae7"
);


export default async (req, res) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "application/json");
    client.messages.create({
        from: "+19048440791",
        to: req.body.to,
        body: req.body.body,
    })
        .then(() => {
            res.send(JSON.stringify({ success: true }));
        })
        .catch((err) => {
            console.log(err);
            res.send(JSON.stringify({ success: false }));
        });
}