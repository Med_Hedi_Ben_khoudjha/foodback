import React, { lazy ,useState,useEffect,useMemo} from 'react'
import { Link} from 'react-router-dom'

import { DocsLink } from 'src/reusable'

import {
  CButton,
  CCollapse,
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow
} from '@coreui/react'
import { Scrollbars } from 'rc-scrollbars';


import CIcon from '@coreui/icons-react'
import { storage,db ,analytics} from "src/Config"

import Search from './Search '
import Pagination from './Pagination'



const ProduitTable = (props) => {

const [search, setSearch] = useState("");
const [currentPage, setCurrentPage] = useState(1);
const [restaurents, setRestaurents] = useState([]);

const [products,setProducts]=useState([])
const [users,setusers]=useState([])
const ITEMS_PER_PAGE = 50;
const [im,setIm]=useState([])
const [images,setImages]=useState()


useEffect(() => {
    fetchBlogs();
    fetchResto();
    getImage()
    fetchuser()
    //var serviceAccount = require("./firebase-service-account");

   
    
    
  }, [])
 const getImage=async()=>{
    const r = storage.ref(`images/Capture.PNG`);

    const u = await r.listAll();
    if(r!==null){
      console.log(u)
      setIm(u)
      
      
    }
  }
  
  const commentsData = useMemo(() => {
    
    let computedComments = products;

    if (search) {
        computedComments = computedComments.filter(
            comment =>
                comment.name.toLowerCase().includes(search.toLowerCase()) ||
                comment.description.toLowerCase().includes(search.toLowerCase())
        );
    }
    
    return computedComments.slice(
        (currentPage - 1) * ITEMS_PER_PAGE,
        (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE
    );
}, [ currentPage, search]);
 


  
  const fetchBlogs=async()=>{
    const response=db.collection('produits');
    const product=[]
    response.onSnapshot(querySnapshot => {


      querySnapshot.forEach((doc) => {
        const { name, description, prix, categorie, url } = doc.data();

        product.push({
          key: doc.id,
          doc,
          name,
          description,
          prix,
          categorie,
          url
        })
        //   setList(products)
        // console.log("Produit"+products.length)
      });
      setProducts(product)
      console.log("Produit" + product.length)

    })
}

const fetchResto=async()=>{
    const response=db.collection('restaurents');
    const restaurent=[]
    response.onSnapshot(querySnapshot => {
  
  
      querySnapshot.forEach((doc) => {
        const { name, description, latitude, longitude, url } = doc.data();
  
        restaurent.push({
          key: doc.id,
          doc,
          name,
          description,
          longitude,
          latitude,
          url
        })
        //   setList(products)
        // console.log("Produit"+products.length)
      });
      setRestaurents(restaurent)
      console.log("restaurents" + restaurent.length)
  
    })
  }
  const fields = ['image','name','email', 'uid', 'createdAt','status',{
    key: 'show_details',
    label: '',
    _style: { width: '1%' },
    sorter: false,
    filter: false
  }]

  const fetchuser=async()=>{
    const response=db.collection('users');
    const users=[]
    response.onSnapshot(querySnapshot => {
  
  
      querySnapshot.forEach((doc) => {
        const { name, createdAt, email, uid, status } = doc.data();
  
        users.push({
          key: doc.id,
          name,
          createdAt,
          email,
          uid,
          status
        })
        //   setList(products)
        // console.log("Produit"+products.length)
      });
      setusers(users)
      console.log("Users" + users.status)
  
    })
  }

  const getBadge = status => {
    switch (status) {
      case 'Active': return 'success'
      case 'Inactive': return 'secondary'
      case 'Pending': return 'warning'
      case 'Banned': return 'danger'
      default: return 'primary'
    }
  }
  const [details, setDetails] = useState([])

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }


  return (
    <>

<CCol xs="12" >
          <CCard>
            <CCardHeader>
              Simple Table
              <DocsLink name="CModal"/>
            </CCardHeader>
            <CCardBody>
            <CDataTable
              items={users}
              fields={fields}
              itemsPerPage={3}
              pagination
              scopedSlots = {{
                'createdAt':
                  (item)=>(
                    <td>
                      <div >
                        <span >{new Date(item.createdAt.seconds*1000).toLocaleDateString()}</span>
                      </div>
                    </td>
                  ),
                  'image':
                  (item)=>(
                    <td>
                      <div className="c-avatar">
                        <img src={`https://firebasestorage.googleapis.com/v0/b/miniprojet-263915.appspot.com/o/photos%2F${item.email}?alt=media&token=986ac546-eaf3-440f-be4e-7d1e63007252`} className="c-avatar-img" alt="admin@bootstrapmaster.com" />
                        <span className="c-avatar-status bg-success"></span>
                      </div>
                    </td>
                  ),
                  'status':
          (item)=>(
            <td>
              <CBadge color={getBadge(item.status)}>
                {item.status}
              </CBadge>
            </td>
          ),'show_details':
          (item, index)=>{
            return (
              <td className="py-2">
                <CButton
                  color="primary"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={()=>{toggleDetails(index)}}
                >
                  {details.includes(index) ? 'Hide' : 'Show'}
                </CButton>
              </td>
              )
          },'details':
          (item, index)=>{
            return (
            <CCollapse show={details.includes(index)}>
              <CCardBody>
                <h4>
                  {item.name}
                </h4>
                <p className="text-muted">User since: {item.email}</p>
                <CButton size="sm" color="info">
                  User Settings
                </CButton>
                <CButton size="sm" color="danger" className="ml-1">
                  Delete
                </CButton>
              </CCardBody>
            </CCollapse>
          )
        }

              }}
            />
            </CCardBody>
          </CCard>
        </CCol>

             
              <CCardBody >
              
 
              <div className="row">
                        <div className="col-md-6">
                            <Pagination
                                total={products}
                                itemsPerPage={ITEMS_PER_PAGE}
                                currentPage={currentPage}
                                onPageChange={page => setCurrentPage(page)}
                            />
                        </div>
                        <div className="col-md-6 d-flex flex-row-reverse">
                            <Search
                                onSearch={value => {
                                    setSearch(value);
                                    setCurrentPage(1);
                                }}
                            />
                        </div>
                    </div>
              <Scrollbars
       
       
       autoHide
       autoHideTimeout={1000}
       autoHideDuration={200}
       autoHeight
       autoHeightMin={0}
       autoHeightMax={500}
       thumbMinSize={30}
       universal={true}
       >
              <table className="table table-hover table-outline mb-0 d-none d-sm-table">
              
                <thead className="thead-light">
                  <tr>
                    <th className="text-center"><CIcon color='#fff876' name="cil-pizza" /></th>
                    <th>Food Name</th>
                    <th>Descripton</th>
                    <th className="text-center">Prix</th>

                    <th>Edit</th>
                    <th>Show</th>
                  </tr>
                </thead>
                
                <tbody>
                {commentsData.map((products,index) => 

            

                  <tr key={products.key} >
                   
                    <td className="text-center">
                      <div className="c-avatar">
                        <img src={`https://firebasestorage.googleapis.com/v0/b/miniprojet-263915.appspot.com/o/images%2F${products.url}?alt=media&token=986ac546-eaf3-440f-be4e-7d1e63007252`} className="c-avatar-img" alt="admin@bootstrapmaster.com" />
                        <span className="c-avatar-status bg-success"></span>
                      </div>
                    </td>
                    <td>
                      <div>{products.name}</div>
                      
                      
                    </td>
                   
                   
                    <td >
                     
                     
                    <div md="3">{products.description}</div>
                    </td>
                    <td >
                    <div>{products.prix} D</div>
                    </td>
                    
                    
                    <td>
                      <div className="Buttons">
                      <Link to ={(`/base/Edit/${products.key}`)}>
                      <CButton   type="submit" size="sm" color="success">  Edit</CButton>
                      </Link>

                      </div>
                    </td>
                    <td>
                    <Link to ={(`/base/Show/${products.key}`)}>
                      <CButton   type="submit" size="sm" color="info">  Show</CButton>
                      </Link>
                    </td>
                  </tr>
                                 )}

                  </tbody>

              </table>
              </Scrollbars>
              </CCardBody>

              <CCardBody >
              
 
 
              <Scrollbars
       
       
       autoHide
       autoHideTimeout={1000}
       autoHideDuration={200}
       autoHeight
       autoHeightMin={0}
       autoHeightMax={300}
       thumbMinSize={30}
       universal={true}
       >
              <table className="table table-hover table-outline mb-0 d-none d-sm-table">
              
                <thead className="thead-light">
                  <tr>
                    <th className="text-center"><CIcon name="cil-restaurant" /></th>
                    <th>RESTO Name</th>
                    <th>Descripton</th>
                    <th>Edit</th>
                    <th>Show</th>
                  </tr>
                </thead>
                
                <tbody>
                {restaurents.map(resto => 

            

                  <tr key={resto.key} >
                   
                    <td className="text-center">
                      <div className="c-avatar">
                        <img src={`https://firebasestorage.googleapis.com/v0/b/miniprojet-263915.appspot.com/o/restaurents%2F${resto.url}?alt=media&token=986ac546-eaf3-440f-be4e-7d1e63007252`} className="c-avatar-img" alt="admin@bootstrapmaster.com" />
                        <span className="c-avatar-status bg-success"></span>
                      </div>
                    </td>
                    <td>
                      <div>{resto.name}</div>
                    </td>
                    <td >
                    <div md="3">{resto.description}</div>
                    </td>
                    
                    <td>
                      <div className="Buttons">
                      <Link to ={(`/base/EditResto/${resto.key}`)}>
                      <CButton   type="submit" size="sm" color="success">  Edit</CButton>
                      </Link>

                      </div>
                    </td>
                    <td>
                    <Link to ={(`/base/ShowResto/${resto.key}`)}>
                      <CButton   type="submit" size="sm" color="info">  Show</CButton>
                      </Link>
                    </td>
                  </tr>
                                 )}

                  </tbody>

              </table>


              </Scrollbars>
              </CCardBody>

         
         
    </>
  )
}

export default ProduitTable
