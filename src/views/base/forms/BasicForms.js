import React, { useState,useEffect,useRef } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow,
  CSwitch
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { DocsLink } from 'src/reusable'

import { useForm } from "react-hook-form";
import { storage, db } from "src/Config"
import ReactMapGL from 'react-map-gl';
import ProduitForm from './produitForm'
import RestoForm from './RestoForm'
const BasicForms = (props) => {

  return (
    <>
        
        <ProduitForm></ProduitForm>
        <RestoForm></RestoForm>
        
    
    </>
  )
}

export default BasicForms
