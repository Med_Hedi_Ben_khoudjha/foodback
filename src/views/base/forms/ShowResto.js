import React, { useState,useEffect,useRef } from 'react'
import { CCard, CCardBody, CCardHeader, CCol, CRow,CButton,CFormGroup,CLabel} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useHistory, useLocation ,Link} from 'react-router-dom'
import { Button } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal'

import { storage, db } from "src/Config"
import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp';
import ReactMapGL, { Marker } from 'react-map-gl';

// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker';
mapboxgl.workerClass = MapboxWorker;
mapboxgl.accessToken = 'pk.eyJ1IjoibWVkY2FiaXN0ZSIsImEiOiJja212OGdxdnQwMnA3MnBtejIyYWRyYXpmIn0.ZkeCXOWf23iAK9-4ECSSjw';

const ShowResto=(props)=>{


    console.log(props.match.params.id)
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [prix, setPrix] = useState();
    const [image, setImage] = useState(null);
    const [url, setUrl] = useState("");
    const [progress, setProgress] = useState(0);
    const [categorie, setCategorie] = useState();
    const [show, setShow] = useState(false);
    const [keys, setKeys] = useState();
    const mapContainer = useRef();
    const [lng, setLng] = useState();
    const [lat, setLat] = useState();
    const [zoom, setZoom] = useState(9);
    const [lngLatm, setLngLatm] = useState();
    var m=null
    const handleClose = () => {setShow(false)
    };
    const handleShow = () => {setShow(true)
      setKeys(props.match.params.id)
      console.log(keys)
    };
    const [products,setProducts]=useState()
    useEffect(() => {
      
      fetchBlogs();
     // getImage()
   

  
    }, [])
    const DeleteProduit=(key)=>{

        db.collection('restaurents')
        .doc(key)
        .delete()
        .then(() => {
          console.log('Produit deleted!');
          setShow(false)
          window.location.reload();
    
        });
      }
     
      useEffect(() => {

       
    }, []);
    const fetchBlogs=async()=>{
      
        
      const product=[]
  
      const response=db.collection('restaurents').doc(props.match.params.id).get()
      .then(documentSnapshot => {
        console.log('User exists: ', documentSnapshot.exists);
    
        if (documentSnapshot.exists) {
          console.log('User data: ', documentSnapshot.data().name);
          setProducts(documentSnapshot.data().name)
          setName(documentSnapshot.data().name)
          setDescription(documentSnapshot.data().description)
          setUrl(documentSnapshot.data().url)
          setLng(documentSnapshot.data().longitude)
          setLat(documentSnapshot.data().latitude)
          setImage(documentSnapshot.data().url)
          const map = new mapboxgl.Map({
            container: mapContainer.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [documentSnapshot.data().longitude, documentSnapshot.data().latitude],
            zoom: zoom
        });
        const nav = new mapboxgl.NavigationControl();
        map.addControl(nav, "top-right");
        new mapboxgl.Marker().setLngLat([documentSnapshot.data().longitude,documentSnapshot.data().latitude]).addTo(map);
      
        return () => map.remove();
        }
      });
     
      console.log(image)
  }
return(
    <CRow>
    
    
 
    <CCol xs="12" sm="6" md="14">
          <CCard>
            <CCardHeader>
            <div className="card-header-actions">
                        <img src={`https://firebasestorage.googleapis.com/v0/b/miniprojet-263915.appspot.com/o/restaurents%2F${url}?alt=media&token=986ac546-eaf3-440f-be4e-7d1e63007252`} className="c-avatar-img" alt="admin@bootstrapmaster.com" />
              </div>
              
            </CCardHeader>
            <CCardBody>
            <div >
             Name: {name}
              </div>             
            
              <div>
             Description: {description}
              </div>

              <CFormGroup row>
                                    <CLabel col md="3" htmlFor="file-input">Logation</CLabel> </CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row>

                                    <CCol xs="12" md="12">
                                        <div className="sidebar">
                                            Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
                                        </div>
                                        <div className="map-container" ref={mapContainer} />
                                    </CCol>
                                </CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            </CCardBody>
            <CCardBody xs="12" sm="6" md="4" >
            <div xs="12" sm="6" md="4">
            <Link to ={(`/base/EditResto/${props.match.params.id}`)}>
                      <CButton   type="submit" size="sm" color="success">  Edit</CButton>
                      </Link>
                      </div>
            <CButton  onClick={handleShow} type="submit" size="sm" color="danger">  Delete</CButton>
            
            </CCardBody>
            
          </CCard>
          </CCol>


          <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>Woohoo, you're sure want to delete!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={()=>{ 
            DeleteProduit(keys);}}>
            Delete produits
          </Button>
        </Modal.Footer>
      </Modal>
  
  </CRow>

)


}
export default ShowResto