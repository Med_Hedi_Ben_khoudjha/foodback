import React, { useState,useEffect,useRef } from 'react'
import { CCard, CCardBody, CCardHeader, CCol, CRow,CButton,} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useHistory, useLocation ,Link} from 'react-router-dom'
import { Button } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal'

import { storage, db } from "src/Config"

const ShowProduit=(props)=>{


    console.log(props.match.params.id)
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [prix, setPrix] = useState();
    const [image, setImage] = useState(null);
    const [url, setUrl] = useState("");
    const [progress, setProgress] = useState(0);
    const [categorie, setCategorie] = useState();
    const [show, setShow] = useState(false);
    const [keys, setKeys] = useState();
  
    const handleClose = () => {setShow(false)
    };
    const handleShow = () => {setShow(true)
      setKeys(props.match.params.id)
      console.log(keys)
    };
    const [products,setProducts]=useState()
    useEffect(() => {
      fetchBlogs();
     // getImage()
    }, [])
    const DeleteProduit=(key)=>{

        db.collection('produits')
        .doc(key)
        .delete()
        .then(() => {
          console.log('Produit deleted!');
          setShow(false)
          window.location.reload();
    
        });
      }
    const fetchBlogs=async()=>{
      const product=[]
  
      const response=db.collection('produits').doc(props.match.params.id).get()
      .then(documentSnapshot => {
        console.log('User exists: ', documentSnapshot.exists);
    
        if (documentSnapshot.exists) {
          console.log('User data: ', documentSnapshot.data().name);
          setProducts(documentSnapshot.data().name)
          setName(documentSnapshot.data().name)
          setPrix(documentSnapshot.data().prix)
          setDescription(documentSnapshot.data().description)
          setUrl(documentSnapshot.data().url)
          setCategorie(documentSnapshot.data().categorie)
          setImage(documentSnapshot.data().url)
        }
      });
     
      console.log(image)
  }
return(
    <CRow>
    
    
 
    <CCol xs="12" sm="6" md="4">
          <CCard>
            <CCardHeader>
              <div className="card-header-actions">
                        <img src={`https://firebasestorage.googleapis.com/v0/b/miniprojet-263915.appspot.com/o/images%2F${url}?alt=media&token=986ac546-eaf3-440f-be4e-7d1e63007252`} className="c-avatar-img" alt="admin@bootstrapmaster.com" />
              </div>
              
            </CCardHeader>
            <CCardBody>
            <div >
             Name: {name}
              </div>
              <div>
             Prix: {prix}
              </div>
              <div>
             Categorie: {categorie}
              </div>
              <div>
             Description: {description}
              </div>
            </CCardBody>
            <CCardBody xs="12" sm="6" md="4" >
            <div xs="12" sm="6" md="4">
            <Link to ={(`/base/Edit/${props.match.params.id}`)}>
                      <CButton   type="submit" size="sm" color="success">  Edit</CButton>
                      </Link>
                      </div>
            <CButton  onClick={handleShow} type="submit" size="sm" color="danger">  Delete</CButton>
            
            </CCardBody>
            
          </CCard>
          </CCol>


          <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>Woohoo, you're sure want to delete!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={()=>{ 
            DeleteProduit(keys);}}>
            Delete produits
          </Button>
        </Modal.Footer>
      </Modal>
  
  </CRow>

)


}
export default ShowProduit