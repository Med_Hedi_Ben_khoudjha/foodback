import React, { useState, useEffect, useRef } from 'react'
import { Link} from 'react-router-dom'

import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CForm,
    CFormGroup,
    CFormText,
    CTextarea,
    CInput,
    CInputFile,    
    CLabel,
    CSelect,
    CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import { useForm } from "react-hook-form";
import { storage, db } from "src/Config"

import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp';
// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker';
mapboxgl.workerClass = MapboxWorker;
mapboxgl.accessToken = 'pk.eyJ1IjoibWVkY2FiaXN0ZSIsImEiOiJja212OGdxdnQwMnA3MnBtejIyYWRyYXpmIn0.ZkeCXOWf23iAK9-4ECSSjw';



const EditResto = (props) => {
 
   

    const mapContainer = useRef();
    const [lng, setLng] = useState(9.9120);
    const [lat, setLat] = useState(37.1911);
    const [zoom, setZoom] = useState(9);
    const [lngLatm, setLngLatm] = useState();
    var m=null

    

    const fetchBlogs=async()=>{

        
        const response=db.collection('restaurents').doc(props.match.params.id).get()
        .then(documentSnapshot => {
          console.log('User exists: ', documentSnapshot.exists);
      
          if (documentSnapshot.exists) {
            console.log('User data: ', documentSnapshot.data().name);
//setProducts(documentSnapshot.data().name)
            setName(documentSnapshot.data().name)
            setDescription(documentSnapshot.data().description)
            setUrl(documentSnapshot.data().url)
            setImage(documentSnapshot.data().url)
            setLat(documentSnapshot.data().latitude)
            setLng(documentSnapshot.data().longitude)
          }
        });
       
        console.log(image)
    }


    useEffect(() => {
        fetchBlogs();
        const map = new mapboxgl.Map({
            container: mapContainer.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [lng, lat],
            zoom: zoom
        });
        map.once('click', addMarker);
            function addMarker(e) {
    
                var marker = new mapboxgl.Marker()
                    .setLngLat(e.lngLat)
                    .addTo(map);
                    setLngLatm(marker.getLngLat())
                    m=marker.getLngLat()
                // Print the marker's longitude and latitude values in the console
                console.log('Longitude: ' + m.lng + ', Latitude: ' + m.lat)
                setLat(m.lat)
                setLng(m.lng)
            }
        const nav = new mapboxgl.NavigationControl();
        map.addControl(nav, "top-right");
        return () => map.remove();
    }, []);

    const [collapsed, setCollapsed] = useState(true)
    const [showElements, setShowElements] = useState(true)
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [prix, setPrix] = useState();
    const [image, setImage] = useState(null);
    const [url, setUrl] = useState("");
    const [progress, setProgress] = useState(0);
    const [categorie, setCategorie] = useState();
    const [place, setPlace] = useState(null);
    const { register, handleSubmit, errors } = useForm();
    const handleRegistration = (data) => console.log(data);
    const handleError = (errors) => { };
    const registerOptions = {
        name: { required: "Name is required" },
        prix: { required: "Prix is required" },
        description: { required: "Description is required" },
        categorie: { required: "Categorie is required" },
        url: { required: "URL is required" },


    };
    const [viewport, setViewport] = useState({
        width: 400,
        height: 400,
        latitude: 37.7577,
        longitude: -122.4376,
        zoom: 8
    });



    const onChange = (val) => {
        setName(val.target.value)
    }
    const handleChange = e => {
        if (e.target.files[0]) {
            setImage(e.target.files[0]);
        }
    };


    //const handleSubmit = (evt) => {
    //evt.preventDefault();
    //alert(`Submitting Name ${name}`)
    //}
    const handleUpload = () => {
        if (name != null) {
            const uploadTask = storage.ref(`restaurents/${image.name}`).put(image);
            uploadTask.on(
                "state_changed",
                snapshot => {
                    const progress = Math.round(
                        (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    );
                    setProgress(progress);
                },
                error => {
                    console.log(error);
                },
                () => {
                    storage
                        .ref(`restaurents/${image.name}`)

                        .getDownloadURL()
                        .then(url => {
                            setUrl(url);
                        });
                }
            );

            db.collection('restaurents').doc(props.match.params.id).update({
                name: name,
                description: description,
                url: image.name,
                longitude:lng,
                latitude:lat

            }).then(documentReference => {
                console.log('document reference ID', documentReference.key)
                console.log()
                props.history.push("/")
            })
                .catch(error => {
                    console.log(error.message)
                })
        }
    };
    console.log("image: ", image);
    return (
        <>
    <CRow>
        <CCol >
         <CCard>
            <CCardHeader>
            Restaurents Form
            <small> Elements</small>
            </CCardHeader>
            <CCardBody>
            <CForm onSubmit={handleSubmit(handleUpload, handleError)} encType="multipart/form-data" className="form-horizontal">
            <CFormGroup row>
            <CCol md="3">
            <CLabel>Static</CLabel>
            </CCol>
            <CCol xs="12" md="9">
            <p className="form-control-static">Restaurent</p>
            </CCol>
           </CFormGroup>
            <CFormGroup row>
            <CCol md="3">
            <CLabel htmlFor="text-input">Name</CLabel>
            </CCol>
            <CCol xs="12" md="9">
            <CInput id="text-input" value={name}
            innerRef={register(registerOptions.name)}
            onChange={(val) => setName(val.target.value)}
            name="name" placeholder="Text" />
            <CFormText><small className="text-danger">
            {errors.name && errors.name.message}
            </small>
            </CFormText>
            </CCol>
            </CFormGroup>
           
           <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="textarea-input">Description</CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CTextarea
                                            type="textarea"
                                            value={description}
                                            innerRef={register(registerOptions.description)}
                                            onChange={(val) => setDescription(val.target.value)}
                                            name="description"
                                        />
                                        <CFormText className="help-block"> <small className="text-danger">
                                            {errors.description && errors.description.message}
                                        </small></CFormText>
                                    </CCol>
                                </CFormGroup>


            <CFormGroup row>
                                    <CLabel col md="3" htmlFor="file-input">File input</CLabel>
                                    <CCol xs="12" md="9">
                                        <CInputFile id="file-input" name="file-input" innerRef={register(registerOptions.url)}
                                            onChange={handleChange} />
                                        <CFormText className="help-block">
                                            <small className="text-danger">
                                                {errors.url && errors.url.message}
                                            </small>
                                        </CFormText>
                                    </CCol>
                                </CFormGroup>
            <CFormGroup row>
                                    <CLabel col md="3" htmlFor="file-input">Logation</CLabel> </CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row>

                                    <CCol xs="12" md="12">
                                        <div className="sidebar">
                                            Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
                                        </div>
                                        <div className="map-container" ref={mapContainer} />
                                    </CCol>
                                </CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            <CFormGroup row></CFormGroup>
            </CForm>
            </CCardBody>
            <CCardFooter>
            <CButton onClick={handleUpload} type="submit" size="sm" color="primary"><CIcon name="cil-scrubber" /> Submit</CButton>
            <Link to ={(`/dashboard`)}>
              <CButton  type="submit" size="sm" color="danger"><CIcon name="cil-ban" /> Annuler</CButton>
                </Link>
                            </CCardFooter>
        </CCard>
        </CCol>
    </CRow>
        </>
    )
}

export default EditResto
