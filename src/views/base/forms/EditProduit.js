import React, { useState, useEffect, useRef } from 'react'
import { Link } from 'react-router-dom'

import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow,
  CSwitch
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { DocsLink } from 'src/reusable'

import { useForm } from "react-hook-form";
import { storage, db } from "src/Config"
import ReactMapGL from 'react-map-gl';
import ProduitForm from './produitForm'
import RestoForm from './RestoForm'
const EditProduit = (props) => {
  console.log(props.match.params.id)
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [prix, setPrix] = useState();
  const [image, setImage] = useState(null);
  const [url, setUrl] = useState("");
  const [progress, setProgress] = useState(0);
  const [categorie, setCategorie] = useState();
  const [place, setPlace] = useState(null);
  const { register, handleSubmit, errors } = useForm();
  const handleRegistration = (data) => console.log(data);
  const [Restau, setRestau] = useState();

  const handleError = (errors) => { };
  const registerOptions = {
    name: { required: "Name is required" },
    prix: { required: "Prix is required" },
    description: { required: "Description is required" },
    categorie: { required: "Categorie is required" },
    url: { required: "URL is required" },


  };
  const [products, setProducts] = useState()
  useEffect(() => {
    fetchBlogs();
    // getImage()
  }, [])
  const fetchBlogs = async () => {
    const product = []

    const response = db.collection('produits').doc(props.match.params.id).get()
      .then(documentSnapshot => {
        console.log('User exists: ', documentSnapshot.exists);

        if (documentSnapshot.exists) {
          console.log('User data: ', documentSnapshot.data().name);
          setProducts(documentSnapshot.data().name)
          setName(documentSnapshot.data().name)
          setPrix(documentSnapshot.data().prix)
          setDescription(documentSnapshot.data().description)
          setUrl(documentSnapshot.data().url)
          setCategorie(documentSnapshot.data().categorie)
          setImage(documentSnapshot.data().url)
          setRestau(documentSnapshot.data().restaurant)
        }
      });

    console.log(image)
  }





  const handleChange = e => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
  };


  //const handleSubmit = (evt) => {
  //evt.preventDefault();
  //alert(`Submitting Name ${name}`)
  //}
  const handleUpload = () => {
    if (name != null) {
      const uploadTask = storage.ref(`images/${image.name}`).put(image);
      uploadTask.on(
        "state_changed",
        snapshot => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          setProgress(progress);
        },
        error => {
          console.log(error);
        },
        () => {
          storage
            .ref(`images/${image.name}`)

            .getDownloadURL()
            .then(url => {
              setUrl(url);
            });
        }
      );

      db.collection('produits').doc(props.match.params.id).update({
        name: name,
        description: description,
        prix: prix,
        categorie: categorie,
        url: image.name,
        restaurant: Restau,

      }).then(documentReference => {
        console.log('document reference ID', documentReference.id)
        console.log()
        props.history.push("/")
      })
        .catch(error => {
          console.log(error.message)
        })
    }
  };
  console.log("image: ", image);
  return (
    <>

      <CRow>
        <CCol xs="12" md="6">
          <CCard>
            <CCardHeader>
              Produits Form
              <small> Elements</small>
            </CCardHeader>
            <CCardBody>
              <CForm onSubmit={handleSubmit(handleUpload, handleError)} encType="multipart/form-data" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel>Static</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <p className="form-control-static">Username</p>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="text-input">Name</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput id="text-input" value={name}
                      innerRef={register(registerOptions.name)}
                      onChange={(val) => setName(val.target.value)}
                      name={"name"} placeholder="Text" />
                    <CFormText><small className="text-danger">
                      {errors.name && errors.name.message}
                    </small>
                    </CFormText>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="email-input">Prix</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="number" name="prix"
                      id="exampleNumber"
                      innerRef={register(registerOptions.prix)}
                      value={prix}
                      onChange={(val) => setPrix(val.target.value)}
                      placeholder="number placeholder" />
                    <CFormText className="help-block"> <small className="text-danger">
                      {errors.prix && errors.prix.message}
                    </small></CFormText>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="textarea-input">Description</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CTextarea
                      type="textarea"
                      value={description}
                      innerRef={register(registerOptions.description)}
                      onChange={(val) => setDescription(val.target.value)}
                      name="description"
                    />
                    <CFormText className="help-block"> <small className="text-danger">
                      {errors.description && errors.description.message}
                    </small></CFormText>
                  </CCol>
                </CFormGroup>

                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="selectSm">Categorie</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CSelect value={categorie}
                      innerRef={register(registerOptions.categorie)}

                      onChange={(val) => setCategorie(val.target.value)}
                      name="categorie" custom size="sm" name="selectSm" id="SelectLm">
                      <option value="0">Please select</option>
                      <option value="Hot Dogs">Hot Dogs</option>
                      <option value="Rice">Rice</option>
                      <option value="Salads">Salads</option>
                      <option value="Burgers">Burgers</option>
                      <option value="Pizza">Pizza</option>
                      <option value="Pasta">Pasta</option>
                    </CSelect>
                    <CFormText className="help-block"> <small className="text-danger">
                      {errors.categorie && errors.categorie.message}
                    </small></CFormText>
                  </CCol>
                </CFormGroup>
                <CCol md="3">
                  <CLabel htmlFor="selectSm">Restaurant</CLabel>
                </CCol>
                <CCol xs="12" md="9">
                  <CCol xs="12" md="9">
                    <CSelect value={Restau}
                      name="restaurant" custom size="sm" name="selectSm" id="SelectLm"
                      onChange={(val) => setRestau(val.target.value)}>
                      <option value="0">Please select</option>
                      <option value="Le Sport Nautique">Le Sport Nautique</option>
                      <option value="Restaurant Le Phenicien">Restaurant Le Phenicien</option>
                      <option value="Resto Ali Mabrouk">Resto Ali Mabrouk</option>

                    </CSelect>

                  </CCol>




                </CCol>

                <CFormGroup row>
                  <CLabel col md="3" htmlFor="file-input">File input</CLabel>
                  <CCol xs="12" md="9">
                    <CInputFile id="file-input" name={"file-input"}
                      innerRef={register(registerOptions.url)}
                      // value={image}
                      onChange={handleChange} />
                    <CFormText className="help-block">
                      <small className="text-danger">
                        {errors.url && errors.url.message}
                      </small>
                    </CFormText>
                  </CCol>
                </CFormGroup>

              </CForm>
            </CCardBody>
            <CCardFooter>
              <CButton onClick={handleUpload} type="submit" size="sm" color="primary"><CIcon name="cil-scrubber" /> Submit</CButton>
              <Link to={(`/dashboard`)}>
                <CButton type="submit" size="sm" color="danger"><CIcon name="cil-ban" /> Annuler</CButton>
              </Link>

            </CCardFooter>
          </CCard>

        </CCol>


      </CRow>


    </>
  )
}

export default EditProduit
