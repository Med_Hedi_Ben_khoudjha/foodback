import React, { useState, useEffect, useRef } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CAlert,
  CProgress,
  CFormGroup,
  CFormText,
  CTextarea,
  CInput,
  CInputFile,
  CLabel,
  CSelect,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useForm } from "react-hook-form";
import { storage, db } from "src/Config"


const ProduitForm = (props) => {
  const [ListResto, setListResto] = useState([]); //Initialise restaurant list with setter
  var restos = [];

  const fetchBlogs = async () => {
    const ref = db.collection("restaurents");

    ref.onSnapshot(querySnapshot => {


      querySnapshot.forEach((doc) => {
        const { name, description, longitude, latitude, categorie, url } = doc.data();

        restos.push({
          key: doc.id,
          doc,
          name,
          description,
          categorie,
          latitude,
          longitude,
          url
        })
        //   setList(products)
        // console.log("Produit"+products.length)
      });
      setListResto(restos)
      console.log("Resto" + restos.length)

    })



  }
  useEffect(() => {
    fetchBlogs();
  }, [])


  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [prix, setPrix] = useState();
  const [image, setImage] = useState(null);
  const [url, setUrl] = useState("");
  const [progress, setProgress] = useState(0);
  const [categorie, setCategorie] = useState();
  const [Restau, setRestau] = useState();
  const [place, setPlace] = useState(null);
  const { register, handleSubmit, errors } = useForm();
  const handleRegistration = (data) => console.log(data);
  const handleError = (errors) => { };
  const registerOptions = {
    name: { required: "Name is required" },
    prix: { required: "Prix is required" },
    description: { required: "Description is required" },
    categorie: { required: "Categorie is required" },
    url: { required: "URL is required" },


  };




  const onChange = (val) => {
    setName(val.target.value)
  }
  const handleChange = e => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
  };


  //const handleSubmit = (evt) => {
  //evt.preventDefault();
  //alert(`Submitting Name ${name}`)
  //}
  const handleUpload = () => {
    if (name != null) {
      const uploadTask = storage.ref(`images/${image.name}`).put(image);
      uploadTask.on(
        "state_changed",
        snapshot => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          setProgress(progress);
        },
        error => {
          console.log(error);
        },
        () => {
          storage
            .ref(`images/${image.name}`)

            .getDownloadURL()
            .then(url => {
              setUrl(url);
            });
        }
      );

      db.collection('produits').add({
        name: name,
        description: description,
        prix: prix,
        categorie: categorie,
        restaurant: Restau,
        rating: 3,
        url: image.name
      }).then(documentReference => {
        console.log('document reference ID', documentReference.id)
        setVisible(3)
        props.history.push("/")
      })
        .catch(error => {
          console.log(error.message)
        })
    }
  };
  console.log("image: ", image);
  const [visible, setVisible] = React.useState(0)

  return (
    <>
      <CRow>
        <CCol xs="12" md="6">
          <CCard>
            <CCardHeader>
              Produits Form
              <small> Elements</small>
              <CAlert
                color="warning"
                show={visible}
                closeButton
                onShowChange={setVisible}
              >
                I will be closed in {visible} seconds!
                <CProgress
                  striped
                  color="warning"
                  value={Number(visible) * 10}
                  size="xs"
                  className="mb-3"
                />
              </CAlert>

            </CCardHeader>
            <CCardBody>
              <CForm onSubmit={handleSubmit(handleUpload, handleError)} encType="multipart/form-data" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel>Static</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <p className="form-control-static">Username</p>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="text-input">Name</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput id="text-input" value={name}
                      innerRef={register(registerOptions.name)}
                      onChange={(val) => setName(val.target.value)}
                      name="name" placeholder="Text" />
                    <CFormText><small className="text-danger">
                      {errors.name && errors.name.message}
                    </small>
                    </CFormText>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="email-input">Prix</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="number" name="prix"
                      id="exampleNumber"
                      innerRef={register(registerOptions.prix)}
                      value={prix}
                      onChange={(val) => setPrix(val.target.value)}
                      placeholder="number placeholder" />
                    <CFormText className="help-block"> <small className="text-danger">
                      {errors.prix && errors.prix.message}
                    </small></CFormText>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="textarea-input">Description</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CTextarea
                      type="textarea"
                      value={description}
                      innerRef={register(registerOptions.description)}
                      onChange={(val) => setDescription(val.target.value)}
                      name="description"
                    />
                    <CFormText className="help-block"> <small className="text-danger">
                      {errors.description && errors.description.message}
                    </small></CFormText>
                  </CCol>
                </CFormGroup>

                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="selectSm">Categorie</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CSelect value={categorie}
                      innerRef={register(registerOptions.categorie)}

                      onChange={(val) => setCategorie(val.target.value)}
                      name="categorie" custom size="sm" name="selectSm" id="SelectLm">
                      <option value="0">Please select</option>
                      <option value="Hot Dogs">Hot Dogs</option>
                      <option value="Rice">Rice</option>
                      <option value="Salads">Salads</option>
                      <option value="Burgers">Burgers</option>
                      <option value="Pizza">Pizza</option>
                      <option value="Pasta">Pasta</option>
                    </CSelect>
                    <CFormText className="help-block"> <small className="text-danger">
                      {errors.categorie && errors.categorie.message}
                    </small></CFormText>
                  </CCol>
                  <CCol md="3">
                    <CLabel htmlFor="selectSm">Restaurant</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CCol xs="12" md="9">
                      <CSelect value={Restau}
                        name="restaurant" custom size="sm" name="selectSm" id="SelectLm"
                        onChange={(val) => setRestau(val.target.value)}>
                        <option value="0">Please select</option>
                        <option value="Le Sport Nautique">Le Sport Nautique</option>
                        <option value="Restaurant Le Phenicien">Restaurant Le Phenicien</option>
                        <option value="Resto Ali Mabrouk">Resto Ali Mabrouk</option>

                      </CSelect>

                    </CCol>




                  </CCol>

                </CFormGroup>

                <CFormGroup row>
                  <CLabel col md="3" htmlFor="file-input">File input</CLabel>
                  <CCol xs="12" md="9">
                    <CInputFile id="file-input" name="file-input" innerRef={register(registerOptions.url)}
                      onChange={handleChange} />
                    <CFormText className="help-block">
                      <small className="text-danger">
                        {errors.url && errors.url.message}
                      </small>
                    </CFormText>
                  </CCol>
                </CFormGroup>

              </CForm>
            </CCardBody>
            <CCardFooter>
              <CButton onClick={handleUpload} type="submit" size="sm" color="primary"><CIcon name="cil-scrubber" /> Submit</CButton>
              <CButton type="reset" size="sm" color="danger"><CIcon name="cil-ban" /> Reset</CButton>
            </CCardFooter>
          </CCard>

        </CCol>


      </CRow>


    </>
  )
}

export default ProduitForm
