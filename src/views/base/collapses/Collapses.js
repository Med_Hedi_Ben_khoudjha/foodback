import React, { useState, useEffect, useRef, useMemo } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CFormText,
  CTextarea,
  CInput,
  CInputFile,
  CLabel,
  CSelect,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import App from './App';

import { useForm } from "react-hook-form";
import { storage, db } from "src/Config"
import { Container, Form, Button } from "react-bootstrap";

import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp';
// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker';
mapboxgl.workerClass = MapboxWorker;
mapboxgl.accessToken = 'pk.eyJ1IjoibWVkY2FiaXN0ZSIsImEiOiJja212OGdxdnQwMnA3MnBtejIyYWRyYXpmIn0.ZkeCXOWf23iAK9-4ECSSjw';
const Commande = (props) => {
  const products = []
  const [List, setList] = useState([]); //Initialise restaurant list with setter



  useEffect(() => {
    console.log("useEffect called");
    const m = (products) => {
      const map = new mapboxgl.Map({
        container: mapContainer.current,
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [lng, lat],
        zoom: zoom,

      });

      //get()
      console.log("Produit" + List.length)
      products.forEach((location) => {
        console.log(location)
        if (location.status == "Non Active") {
          new mapboxgl.Marker({
            color: "red",
            draggable: true
          })
            .setLngLat([location.longitude, location.latitude])
            .setPopup(new mapboxgl.Popup({ offset: 30 })
              .setHTML('<p style="color:red">' + '<h4>' + location.name + '</h4>' + '<br />' + location.email + '</h4>' + '<br />' + location.phone + + '</h4>' + '<br />' + location.createdAt + '</p>'))
            .addTo(map);
        } else {
          new mapboxgl.Marker({
            color: "green",
            draggable: true
          })
            .setLngLat([location.longitude, location.latitude])
            .setPopup(new mapboxgl.Popup({ offset: 30 })
              .setHTML('<h4>' + location.name + '</h4>' + '<br />' + location.email + '</h4>' + '<br />' + location.phone + '</h4>' + '<br />' + location.createdAt))
            .addTo(map);
        }

      })


      // map.once('click', addMarker);

      const nav = new mapboxgl.NavigationControl();
      map.addControl(nav, "top-right");
      return () => map.remove();

    }
    const getWeather = () => {
      const ref = db.collection("paiement users");

      ref.onSnapshot(querySnapshot => {


        querySnapshot.forEach((doc) => {
          const { name, email, prix, array, latitude, longitude, createdAt, quantite, phone, status } = doc.data();

          products.push({
            key: doc.id,
            doc,
            name,
            email,
            prix,
            array,
            latitude,
            longitude,
            createdAt,
            quantite,
            phone,
            status
          })
          //   setList(products)
          // console.log("Produit"+products.length)
        });
        setList(products)
        m(products)
      })
    };
    getWeather();

  }, []);
  console.log(List);



  const mapContainer = useRef();
  const [lng, setLng] = useState(9.8711995);
  const [lat, setLat] = useState(37.2722081);
  const [zoom, setZoom] = useState(15);
  const [lngLatm, setLngLatm] = useState();



  useEffect(() => {



  }, []);

  const [collapsed, setCollapsed] = useState(true)
  const [showElements, setShowElements] = useState(true)
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [prix, setPrix] = useState();
  const [image, setImage] = useState(null);
  const [url, setUrl] = useState("");
  const [progress, setProgress] = useState(0);
  const [categorie, setCategorie] = useState();
  const [place, setPlace] = useState(null);
  const { register, handleSubmit, errors } = useForm();
  const handleRegistration = (data) => console.log(data);
  const handleError = (errors) => { };
  const registerOptions = {
    name: { required: "Name is required" },
    prix: { required: "Prix is required" },
    description: { required: "Description is required" },
    categorie: { required: "Categorie is required" },
    url: { required: "URL is required" },


  };
  const [viewport, setViewport] = useState({
    width: 400,
    height: 400,
    latitude: 37.7577,
    longitude: -122.4376,
    zoom: 8
  });



  const onChange = (val) => {
    setName(val.target.value)
  }
  const handleChange = e => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
  };


  //const handleSubmit = (evt) => {
  //evt.preventDefault();
  //alert(`Submitting Name ${name}`)
  //}
  const handleUpload = () => {
    if (name != null) {
      const uploadTask = storage.ref(`restaurents/${image.name}`).put(image);
      uploadTask.on(
        "state_changed",
        snapshot => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          setProgress(progress);
        },
        error => {
          console.log(error);
        },
        () => {
          storage
            .ref(`restaurents/${image.name}`)

            .getDownloadURL()
            .then(url => {
              setUrl(url);
            });
        }
      );

      db.collection('restaurents').add({
        name: name,
        description: description,
        url: image.name,
        longitude: lng,
        latitude: lat

      }).then(documentReference => {
        console.log('document reference ID', documentReference.id)
        console.log()
        props.history.push("/")
      })
        .catch(error => {
          console.log(error.message)
        })
    }
  };
  console.log("image: ", image);
  return (
    <>
      <CRow>
        <CCol >
          <CCard>
            <CCardHeader>
              Restaurents Form
              <small> Elements</small>
            </CCardHeader>
            <CCardBody>
              <CForm onSubmit={handleSubmit(handleUpload, handleError)} encType="multipart/form-data" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel>Static</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <p className="form-control-static">Restaurent</p>
                  </CCol>
                </CFormGroup>


                <App></App>


                <CFormGroup row>
                  <CLabel col md="3" htmlFor="file-input">Logation</CLabel> </CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row>

                  <CCol xs="12" md="12">
                    <div className="sidebar">
                      Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
                    </div>
                    <div className="map-container" ref={mapContainer} />
                  </CCol>
                </CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
                <CFormGroup row></CFormGroup>
              </CForm>
            </CCardBody>
            <CCardFooter>
              <CButton onClick={handleUpload} type="submit" size="sm" color="primary"><CIcon name="cil-scrubber" /> Submit</CButton>
              <CButton type="reset" size="sm" color="danger"><CIcon name="cil-ban" /> Reset</CButton>
            </CCardFooter>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default Commande
