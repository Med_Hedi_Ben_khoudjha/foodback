import React, { createContext, useState, useEffect } from 'react';

import { storage, db } from "src/Config"


export const AuthContext = createContext({});
export const AuthProvider = ({ children }) => {

    const [List, setList] = useState([]); //Initialise restaurant list with setter
    const products = []


    return (
        <AuthContext.Provider
            value={{


                List,
                products,
                getCommande: async () => {

                    const ref = db.collection("paiement users");

                    ref.onSnapshot(querySnapshot => {


                        querySnapshot.forEach((doc) => {
                            const { name, email, prix, array, latitude, longitude, createdAt, quantite, phone, status } = doc.data();

                            products.push({
                                key: doc.id,
                                doc,
                                name,
                                email,
                                prix,
                                array,
                                latitude,
                                longitude,
                                createdAt,
                                quantite,
                                phone,
                                status
                            })
                            //   setList(products)
                            // console.log("Produit"+products.length)
                        });
                        setList(products)
                        console.log("Produit" + products.length)
                    })
                }




            }}
        >
            {children}
        </AuthContext.Provider >
    );
};
