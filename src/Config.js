import firebase from 'firebase/app'
import 'firebase/storage'
import 'firebase/firestore'
import "firebase/auth";
import 'firebase/analytics';

const setting={timestampsInSnapshots:true}

var firebaseConfig = {
    apiKey: "AIzaSyB-XUilH0Z2hyXiYY0Gaw3y5bymNZrylF4",
    authDomain: "miniprojet-263915.firebaseapp.com",
    projectId: "miniprojet-263915",
    storageBucket: "miniprojet-263915.appspot.com",
    messagingSenderId: "1056257770376",
    appId: "1:1056257770376:web:847a66564696fdff48f03d",
    measurementId: "G-7TLJ1TMC99"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
 // firebase.analytics();
  firebase.firestore().settings(setting);

  const storage = firebase.storage()
  const db = firebase.firestore()
  const auth = firebase.auth()
  const analytics =firebase.analytics()

  export  {
    storage,db,auth,analytics, firebase as default
  }